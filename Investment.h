#include <iostream> 
using namespace std;

class Investment 
 {
    private:  //properties
         long double value;
         float interest_rate; 
         


    public:  //methods
        
         Investment();//default constructor
	 Investment(long double, float); 

          void setValue(long double);
          long double getValue();
          void setInterest_rate(float);
          float  getInterest_rate();

 };

