#include <iostream>
#include "Investment.h"

using namespace std;

 Investment::Investment
{
  value = 0;
  interest_rate = 0.0;
}

 Investment::Investment( long double val, float rate)
{
  value = val;
  interest_rate = rate;
}

 void Investment::setValue(long double val)
 {
   value = val;
 }

 long double Investment::getValue()
 {
  return value;
 }

 void Investment::setInterest_rate(float rate)
{
 interest_rate = rate;
}

 float Investment::getInterest_rate()
{
 return interest_rate;
}
